# README #

This is the repository for the shared notes developed by J. Gleason and A. Vinod for the **Probability Theory: Independence, Interchangeability and Martingales**.

### What is this repository for? ###

* This public repository is aimed at collecting our thoughts/notes on the textbook.

### Where do I get the notes? ###

* You can find the notes [here](https://bitbucket.org/abyvinod/sharednotes/raw/master/main.pdf).

### Who do I talk to for more details? ###

* Abraham P. Vinod (aby [dot] vinod @ gmail [dot] com)